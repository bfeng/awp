!include MUI2.nsh

;--------------------------------
;General
Name "Full Screen Writer"
OutFile "setup.exe"
InstallDir "$PROGRAMFILES\FullScreenWriter"

;--------------------------------
;Interface Settings
!define MUI_ABORTWARNING

;--------------------------------
;Pages
!insertmacro MUI_PAGE_WELCOME
!insertmacro MUI_PAGE_LICENSE "${NSISDIR}\Docs\Modern UI\License.txt"
!insertmacro MUI_PAGE_DIRECTORY
!insertmacro MUI_PAGE_INSTFILES
!insertmacro MUI_PAGE_FINISH

;--------------------------------
;Languages
!insertmacro MUI_LANGUAGE "English"

Section

SetOutPath $INSTDIR

File "startup.exe"
File "FullScreenWriter.exe"
File "libgcc_s_dw2-1.dll"
File "mingwm10.dll"
File "QtCore4.dll"
File "QtGui4.dll"
File "startup.ico"

WriteUninstaller "$INSTDIR\Uninstall.exe"

WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\FullScreenWriter" \
                 "DisplayName" "Full Screen Writer"
WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\FullScreenWriter" \
                 "UninstallString" "$\"$INSTDIR\Uninstall.exe$\""
				 
CreateShortCut "$DESKTOP\Full Screen Writer.lnk" "$INSTDIR\startup.exe" \
				"" "$INSTDIR\startup.ico" 0

SectionEnd

Section "Uninstall"

Delete "$INSTDIR\startup.ico"
Delete "$INSTDIR\QtGui4.dll"
Delete "$INSTDIR\QtCore4.dll"
Delete "$INSTDIR\mingwm10.dll"
Delete "$INSTDIR\libgcc_s_dw2-1.dll"
Delete "$INSTDIR\FullScreenWriter.exe"
Delete "$INSTDIR\startup.exe"
Delete "$INSTDIR\Uninstall.exe"

Delete "$DESKTOP\Full Screen Writer.lnk"

DeleteRegKey HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\FullScreenWriter"

SectionEnd