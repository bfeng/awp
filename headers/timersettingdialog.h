#ifndef TIMERSETTINGDIALOG_H
#define TIMERSETTINGDIALOG_H

#include <QDialog>
#include <QTime>
#include <QTimer>

namespace Ui {
    class TimerSettingDialog;
}

class TimerSettingDialog : public QDialog
{
    Q_OBJECT

public:
    explicit TimerSettingDialog(QWidget *parent = 0);
    ~TimerSettingDialog();
    QTime getSettingTime();

public slots:
    void showTime();

private:
    Ui::TimerSettingDialog *ui;
    QTimer * timer;
};

#endif // TIMERSETTINGDIALOG_H
