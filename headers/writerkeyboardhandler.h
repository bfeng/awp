#ifndef WRITERKEYBOARDHANDLER_H
#define WRITERKEYBOARDHANDLER_H

#include <QObject>
#include <QKeyEvent>
#include <QKeySequence>

class WriterKeyboardHandler : public QObject
{
    Q_OBJECT
public:
    explicit WriterKeyboardHandler(QObject *parent = 0);
    bool closeLock;
    bool keyboardLock;

signals:

protected:
    bool eventFilter(QObject *, QEvent *);

private:
    bool acceptKey(int keyCode);
};

#endif // WRITERKEYBOARDHANDLER_H
