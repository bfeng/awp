#ifndef WINDOWS_KEYBOARD_HOOKER_H
#define WINDOWS_KEYBOARD_HOOKER_H

#include "windows.h"

HHOOK hKeyboardHook;
HINSTANCE hInst;

LRESULT CALLBACK LowLevelKeyboardProc(INT nCode, WPARAM wParam, LPARAM lParam)
{
    PKBDLLHOOKSTRUCT p;

    if (nCode == HC_ACTION)
    {
        p = (PKBDLLHOOKSTRUCT) lParam;
        if (
                // WIN key (for Start Menu)
                ((p->vkCode == VK_LWIN) || (p->vkCode == VK_RWIN)) ||
                // ALT+TAB
                (p->vkCode == VK_TAB && p->flags & LLKHF_ALTDOWN) ||
                // ALT+ESC
                (p->vkCode == VK_ESCAPE && p->flags & LLKHF_ALTDOWN) ||
                // CTRL+ESC
                ((p->vkCode == VK_ESCAPE) && ((GetAsyncKeyState(VK_CONTROL) & 0x8000) != 0)) ||
                // CTRL+SHIFT+ESC
                ((p->vkCode == VK_ESCAPE) && ((GetAsyncKeyState(VK_CONTROL) & 0x8000) != 0) && ((GetAsyncKeyState(VK_SHIFT) & 0x8000) != 0)) ||
                // CTRL+ALT+DEL (Unfortunately doesn't work !)
                ((p->vkCode == VK_DELETE) && ( (p->flags & LLKHF_ALTDOWN) != 0 ) && ( (GetAsyncKeyState(VK_CONTROL) & 0x8000) != 0))
                )
            return 1;
    }

    return CallNextHookEx(hKeyboardHook, nCode, wParam, lParam);
}

/**
    put into main function
 */
//    WId hWnd = w.winId();
//    hInst = (HINSTANCE)GetWindowLong(hWnd, GWL_HINSTANCE);
//    SetWindowsHookEx(
//            WH_KEYBOARD_LL,
//            LowLevelKeyboardProc,
//            hInst,
//            0);

#endif // WINDOWS_KEYBOARD_HOOKER_H
