#ifndef WRITERMAINWINDOW_H
#define WRITERMAINWINDOW_H

#include <QMainWindow>
#include <QTime>
#include <QTimer>
#include <QFile>
#include <QDir>
#include <QTextStream>
#include <QMessageBox>
#include <QFileDialog>
#include "headers/timersettingdialog.h"
#include "headers/writerkeyboardhandler.h"

namespace Ui {
    class WriterMainWindow;
}

class WriterMainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit WriterMainWindow(QWidget *parent = 0);
    ~WriterMainWindow();
    void closeEvent(QCloseEvent *);
    void setTime(const QTime time);
    void setupStyle();

protected:
    QString fileToStr(QString filename);
    QString getConfHelper(QString, QString);

private slots:
    void start();
    void done();
    void quit();
    void updateTimeCount();
    void countdownQuit();
    void showDialog();
    void writeTempFile();
    bool saveFile();
    void lockButtons(const bool lock);
    void writeFile(const QString fileName);
    //void lockScreen();
    void countWords();

private:
    static const int quitSeconds = 3;

    Ui::WriterMainWindow *ui;
    QTimer *timer;
    QTimer *quitTimer;
    QTime endTime;
    QFile * tempFile;
    QTimer * autoSave;
    WriterKeyboardHandler *handler;
    QMessageBox *quitMessageBox;
    int quitCountdown;

    void adjustUi();

};

#endif // WRITERMAINWINDOW_H
