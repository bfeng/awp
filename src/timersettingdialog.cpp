#include "headers/timersettingdialog.h"
#include "ui_timersettingdialog.h"

TimerSettingDialog::TimerSettingDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::TimerSettingDialog)
{
    ui->setupUi(this);
    showTime();
    timer = new QTimer();
    connect(timer, SIGNAL(timeout()), this, SLOT(showTime()));
    timer->setInterval(1000);
    timer->start();
}

TimerSettingDialog::~TimerSettingDialog()
{
    delete timer;
    delete ui;
}

QTime TimerSettingDialog::getSettingTime()
{
    return ui->timeEdit->time();
}

void TimerSettingDialog::showTime()
{
    QTime time = QTime::currentTime();
    QString text = time.toString("hh:mm:ss");
    ui->timeLabel->setText(text);
}
