#include <QScreen>
#include <QSizePolicy>
#include "headers/writermainwindow.h"
#include "ui_writermainwindow.h"

WriterMainWindow::WriterMainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::WriterMainWindow)
{
    ui->setupUi(this);
    ui->saveButton->hide();
    ui->startButton->setEnabled(false);

    this->adjustUi();

    QString filepath = QString("%1/%2.tmp")
                       .arg(QDir::current().absolutePath())
                       .arg(QTime::currentTime().msec());
    tempFile = new QFile(filepath);

    timer = new QTimer();
    timer->setInterval(1000);
    connect(timer, SIGNAL(timeout()), this, SLOT(updateTimeCount()));

    connect(ui->settingButton, SIGNAL(clicked()), this, SLOT(showDialog()));
    connect(ui->startButton, SIGNAL(clicked()), this, SLOT(start()));
    connect(ui->saveButton, SIGNAL(clicked()), this, SLOT(saveFile()));
    connect(ui->quitButton, SIGNAL(clicked()), this, SLOT(quit()));
    connect(ui->textEdit, SIGNAL(textChanged()), this, SLOT(countWords()));

    autoSave = new QTimer();
    connect(autoSave, SIGNAL(timeout()), this, SLOT(writeTempFile()));
    autoSave->setInterval(1000*60);

    handler = new WriterKeyboardHandler(this);
    this->installEventFilter(handler);
    ui->textEdit->installEventFilter(handler);
    this->grabKeyboard();
}

WriterMainWindow::~WriterMainWindow()
{
    delete ui;
}

void WriterMainWindow::adjustUi()
{
    QScreen *screen = QGuiApplication::primaryScreen();
    QRect screenRect = screen->geometry();
    int aviWidth = screenRect.width();
    if(aviWidth > 800)
    {
        QRect editRect = ui->centralFrame->geometry();
        if(aviWidth > 1024)
            editRect.setWidth(screenRect.width() * 0.6);
        else
            editRect.setWidth(960);
        ui->textEdit->setMinimumWidth(editRect.width());
        ui->textEdit->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Expanding);
        ui->textEdit->updateGeometry();
    }
}

void WriterMainWindow::setupStyle()
{
    // Open config and stylesheet files
       QString style, conf;
       try {
           style = this->fileToStr("style.qss");
           conf = this->fileToStr("config.cfg");
       } catch (QString) {
           qDebug("files not found!");
           style = "";
           conf = "";
       }
//       // Get width
//       int widthPercentage = this->getConfHelper("Width-percentage:",conf).toInt();
//       // Get tab-width
//       int tabWidth = this->getConfHelper("Tab-width:",conf).toInt();
//       // Set configurations
//       textField->setTabStopWidth(tabWidth);                   // Tab width
//       textLayout->setStretch(0,(100-widthPercentage)/2);      // Set spacing stretch
//       textLayout->setStretch(1,widthPercentage);              // Set textField stretch
//       textLayout->setStretch(2,(100-widthPercentage)/2);      // Set spacing stretch
       // Get and set colors
       style.replace("BGCOLOR",this->getConfHelper("Background-color:",conf));
       style.replace("SCCOLOR",this->getConfHelper("Scroll-color:",conf));
       style.replace("LACOLOR",this->getConfHelper("Light-app-color:",conf));
       style.replace("DACOLOR",this->getConfHelper("Dark-app-color:",conf));
       style.replace("FCOLOR",this->getConfHelper("Font-color:",conf));
       style.replace("SCOLOR",this->getConfHelper("Selected-font-color:",conf));
       style.replace("SBCOLOR",this->getConfHelper("Selected-background-color:",conf));
       // Get font styles
       style.replace("FFAMILY",this->getConfHelper("Font:",conf));
       style.replace("FSIZE",this->getConfHelper("Font-size:",conf));
       style.replace("FBOLD",( (this->getConfHelper("Font-bold:",conf) == "true") ? "bold" : "normal" ));
       style.replace("FITALIC",( (this->getConfHelper("Font-italic:",conf) == "true") ? "italic" : "normal" ));
       style.replace("FUNDERLINE",( (this->getConfHelper("Font-underline:",conf) == "true") ? "underline" : "none" ));
       // Apply style
       qApp->setStyleSheet(style);
}

QString WriterMainWindow::getConfHelper(QString str, QString conf){
    int n, m;
    m = conf.indexOf(str);
    n = conf.indexOf(":",m)+1;
    m = conf.indexOf("\n",n);
    return conf.mid(n,m-n).trimmed();
}

bool WriterMainWindow::saveFile()
{
    QString fileName = QFileDialog::getSaveFileName(this, tr("Save File"),
                                                    QDir::homePath(),
                                                    tr("Text (*.txt)"));

    if(fileName.isEmpty()) {
        ui->saveButton->show();
        return false;
    }

    writeFile(fileName);
    ui->quitButton->show();
    return true;
}

void WriterMainWindow::start()
{
    QMessageBox msgBox(this);
    msgBox.setText(QString("Your end time is at <font color=red>%1:%2:%3</font>")
                   .arg(endTime.hour(), 2, 10, QChar('0'))
                   .arg(endTime.minute(), 2, 10, QChar('0'))
                   .arg(endTime.second(), 2, 10, QChar('0')));
    msgBox.setInformativeText("Make sure you wanna start?");
    msgBox.setStandardButtons(QMessageBox::Ok | QMessageBox::Cancel);
    msgBox.setDefaultButton(QMessageBox::Cancel);
    int ret = msgBox.exec();
    switch(ret) {
    case QMessageBox::Cancel:
        break;
    case QMessageBox::Ok: {
            lockButtons(true);
            this->releaseKeyboard();
            ui->textEdit->grabKeyboard();
            handler->keyboardLock = false;
            timer->start();
            autoSave->start();
            break;
        }
    default:break;
    }
}

void WriterMainWindow::done()
{
    ui->textEdit->releaseKeyboard();
    this->grabKeyboard();
    handler->keyboardLock = true;

    writeTempFile();
    lockButtons(false);

    if(saveFile()) {
        handler->closeLock = false;
        this->close();
    }
}

void WriterMainWindow::writeFile(const QString fileName)
{
    QString text = ui->textEdit->toPlainText();
    QFile save(fileName);
    if(!save.open(QIODevice::WriteOnly | QIODevice::Text))
        return;
    QTextStream out(&save);
    out << text;
    out.flush();
    save.close();
}

void WriterMainWindow::quit()
{
    quitTimer = new QTimer();
    quitCountdown = WriterMainWindow::quitSeconds;
    quitMessageBox = new QMessageBox(this);
    quitMessageBox->setWindowModality(Qt::ApplicationModal);

    quitMessageBox->setText("You wanna quit?");
    quitMessageBox->setInformativeText(QString("%1").arg(quitCountdown));
    quitMessageBox->addButton(QMessageBox::Cancel);
    quitMessageBox->setDefaultButton(QMessageBox::Cancel);
    quitMessageBox->setVisible(true);
    quitMessageBox->show();
    quitMessageBox->raise();

    connect(quitTimer, SIGNAL(timeout()), this, SLOT(countdownQuit()));
    quitTimer->start(1000);

    if(quitMessageBox->exec() == QMessageBox::Cancel)
    {
        quitCountdown = WriterMainWindow::quitSeconds;
        quitTimer->disconnect();
        quitTimer->stop();
        free(quitTimer);
        return;
    }
}

void WriterMainWindow::countdownQuit()
{
    quitCountdown--;
    quitMessageBox->setInformativeText(QString("%1").arg(quitCountdown));

    if(quitCountdown<=0) {
        handler->closeLock = false;
        this->close();
    }
}

void WriterMainWindow::writeTempFile()
{
    QString text = ui->textEdit->toPlainText();
    if(!tempFile->open(QIODevice::WriteOnly | QIODevice::Text))
        return;
    QTextStream out(tempFile);
    out << text;
    tempFile->close();
}

void WriterMainWindow::setTime(const QTime time)
{
    this->endTime = time;
    ui->startButton->setEnabled(true);
}

void WriterMainWindow::updateTimeCount()
{
    int countdown = QTime::currentTime().secsTo(endTime);

    if(countdown <-1) {
        countdown += 3600*24;
    } else if(countdown <= 0) {
        timer->stop();
        ui->label->setText("00:00:00");
        done();
        return;
    }

    int timeInt[3] = {0,0,0};

    if(countdown >= 3600) {
        timeInt[0] = countdown/3600;
        countdown = countdown%3600;
    }
    if(countdown >= 60) {
        timeInt[1] = countdown/60;
        countdown = countdown%60;
    }
    if(countdown >= 0){
        timeInt[2] = countdown;
    }

    QString text = QString("%1:%2:%3")
                   .arg(timeInt[0], 2, 10, QChar('0'))
                   .arg(timeInt[1], 2, 10, QChar('0'))
                   .arg(timeInt[2], 2, 10, QChar('0'));

    ui->label->setText(text);
}

void WriterMainWindow::lockButtons(const bool lock)
{
    ui->settingButton->setEnabled(!lock);
    ui->startButton->setEnabled(!lock);
    ui->saveButton->setEnabled(!lock);
    ui->quitButton->setEnabled(!lock);

    if(lock) {
        ui->settingButton->setVisible(false);
        ui->startButton->setVisible(false);
        ui->saveButton->setVisible(false);
        ui->quitButton->setVisible(false);
    }
}

void WriterMainWindow::showDialog()
{
    this->releaseKeyboard();
    this->handler->keyboardLock = false;

    TimerSettingDialog dialog(this);
    dialog.setModal(true);
    dialog.setVisible(true);
    dialog.show();
    dialog.raise();
    dialog.exec();
    setTime(dialog.getSettingTime());

    this->handler->keyboardLock = true;
    this->grabKeyboard();
}

QString WriterMainWindow::fileToStr(QString filename)
{
    // Open file
    QFile file(filename);
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text)) throw QString("Error opening file");
    // Create stream and load data
    QTextStream in(&file);
    QString str = "";
    while (!in.atEnd()) {
        QString line = in.readLine();
        str.append(line);
        str.append("\n");
    }
    file.close();
    // Return string
    return str;
}

void WriterMainWindow::closeEvent(QCloseEvent *event)
{
    QMainWindow::closeEvent(event);
}

void WriterMainWindow::countWords()
{
    int wordCount = ui->textEdit->toPlainText()
            .split(QRegExp("(\\s|\\n|\\r)+")
                   , QString::SkipEmptyParts).count();
    QString words = QString("Words: %1").arg(wordCount);
    ui->wordCounter->setText(words);
}
