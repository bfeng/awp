#include <QApplication>
#include "headers/writermainwindow.h"
#include <QDebug>

#ifdef Q_OS_WIN
#include "windows.h"
#endif


int main(int argc, char *argv[])
{

#ifdef Q_OS_WIN
    // Save original desktop
    HDESK hOriginalThread = GetThreadDesktop(GetCurrentThreadId());
    HDESK hOriginalInput = OpenInputDesktop(0, FALSE, DESKTOP_SWITCHDESKTOP);

    // Create a new Desktop and switch to it
    HDESK hNewDesktop = CreateDesktop(TEXT("Writer Desktop"), NULL, NULL, 0, GENERIC_ALL, NULL);
    SetThreadDesktop(hNewDesktop);
    SwitchDesktop(hNewDesktop);
#endif

    // Execute thread/process in the new desktop
    QApplication a(argc, argv);
    int result;
    WriterMainWindow w;
    w.setupStyle();

#ifdef QT_DEBUG
    w.showMaximized();
#else
    w.showFullScreen();
#endif

    result = a.exec();

#ifdef Q_OS_WIN
    qDebug("quit with code %d", result);

    // Restore original desktop
    SwitchDesktop(hOriginalInput);
    SetThreadDesktop(hOriginalThread);

    // Close the Desktop
    CloseDesktop(hNewDesktop);
#endif

    return result;
}
