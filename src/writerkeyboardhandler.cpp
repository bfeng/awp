#include "headers/writerkeyboardhandler.h"

WriterKeyboardHandler::WriterKeyboardHandler(QObject *parent) :
    QObject(parent)
{
    closeLock = true;
    keyboardLock = true;
}

bool WriterKeyboardHandler::eventFilter(QObject *obj, QEvent *event)
{

//    switch(event->type())
//    {
//    case QEvent::Close: {
//        if(closeLock) {
//            return true;
//        }
//        break;
//    }
//    case QEvent::KeyPress:
//    case QEvent::KeyRelease:
//    case QEvent::Shortcut:
//    case QEvent::ShortcutOverride:
//    case QEvent::KeyboardLayoutChange:
//    case QEvent::InputMethod: {
//        if(keyboardLock) {
//            return true;
//        }
//        break;
//    }
//    case QEvent::MouseButtonDblClick:
//    case QEvent::MouseButtonPress:
//    case QEvent::MouseButtonRelease:
//    case QEvent::MouseMove:
//    case QEvent::MouseTrackingChange:
//    default: QObject::eventFilter(obj, event);
//    }
    if(event->type() == QEvent::KeyPress)
    {
        QKeyEvent *keyEvent = static_cast<QKeyEvent *>(event);
        int keyCode = keyEvent->key();
        if(!this->acceptKey(keyCode))
        {
            qDebug("Ate key press %x", keyEvent->key());
            return true;
        }
    }
    return QObject::eventFilter(obj, event);
}

bool WriterKeyboardHandler::acceptKey(int keyCode)
{
    if(
            keyCode == Qt::Key_Space ||
            keyCode == Qt::Key_Comma ||
            keyCode == Qt::Key_Period ||
            keyCode == Qt::Key_Apostrophe ||
            keyCode == Qt::Key_Colon ||
            keyCode == Qt::Key_Semicolon ||
            keyCode == Qt::Key_Minus ||
            keyCode == Qt::Key_Plus ||
            keyCode == Qt::Key_Question || keyCode == Qt::Key_Slash ||
            keyCode == Qt::Key_Backslash ||
            keyCode == Qt::Key_BracketLeft || keyCode == Qt::Key_BracketRight ||
            keyCode == Qt::Key_QuoteLeft ||
            keyCode == Qt::Key_Return ||
            keyCode == Qt::Key_Backspace ||
            keyCode == Qt::Key_Shift ||
            keyCode == Qt::Key_CapsLock ||
            (keyCode >= Qt::Key_Left && keyCode <= Qt::Key_Down) ||
            (keyCode >= Qt::Key_0 && keyCode <= Qt::Key_9) ||
            (keyCode >= Qt::Key_A && keyCode <= Qt::Key_Z)
      )
        return true;
    else
        return false;
}
