#-------------------------------------------------
#
# Project created by QtCreator 2010-12-05T16:22:11
#
#-------------------------------------------------

# Copies the given files to the destination directory
defineTest(copyToDestdir) {
    files = $$1

    for(FILE, files) {
        DDIR = $$DESTDIR
        FILE = $$PWD/$$FILE

        # Replace slashes in paths with backslashes for Windows
        win32:FILE ~= s,/,\\,g
        win32:DDIR ~= s,/,\\,g

        QMAKE_POST_LINK += $$QMAKE_COPY $$quote($$FILE) $$quote($$DDIR) $$escape_expand(\\n\\t)
    }

    export(QMAKE_POST_LINK)
}

QT       += core gui
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = AWP
TEMPLATE = app

SOURCES += src/main.cpp\
           src/writermainwindow.cpp \
           src/timersettingdialog.cpp \
           src/writerkeyboardhandler.cpp

HEADERS  += headers/writermainwindow.h \
            headers/timersettingdialog.h \
            headers/writerkeyboardhandler.h \
            headers/windows_keyboard_hooker.h

FORMS    += forms/writermainwindow.ui \
            forms/timersettingdialog.ui

OTHER_FILES += \
    src/config.cfg

CONFIG(release, debug|release): DESTDIR = $$OUT_PWD/release
CONFIG(debug, debug|release): DESTDIR = $$OUT_PWD/debug

copyToDestdir($$OTHER_FILES)

DISTFILES += \
    src/style.css
